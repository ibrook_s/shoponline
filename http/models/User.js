const mongoose = require('mongoose');
const User = mongoose.model('User', {
    name: {
        type: String,
        required: true
    },
    login: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    email: String,
    phone: {
        type: Number,
        required: true
    },
    token: String,
    cityId: Number
});

module.exports = User;