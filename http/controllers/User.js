const User = require('../models/User.js');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
module.exports = {
    get(ctx, next) {
        //Проверка бд
        const newUser = new User({
            name: 'Рома',
            login: 'rnskv',
            password: 'aaa111'
        });

        newUser.save().then((data) => {
            console.log(data)
        });
    },
    async login(ctx) {
        const params = ctx.request.body;
        const user = await User.findOne({
            login: params.login
        });
        if (user.password === params.password) {
            const token = jwt.sign({
                    name: params.name,
                    login: params.login
                },
                'secret',
                {
                    expiresIn: '1h'
                });
            ctx.body = JSON.stringify({
                token
            });
        } else {
            ctx.body = JSON.stringify({
                statusCode: 0,
                statusMessage: 'Пользователь не авторизирован.'
            })
        }
    },
    async add(ctx) {
        const params = ctx.request.body;
        console.log(params);
        const user = new User({
            name: params.name,
            login: params.login,
            password: params.password,
            email: params.email,
            phone: params.phone,
        });

        try {
            await user.save();
            ctx.status = 201
        } catch (err) {
            ctx.body = JSON.stringify({
                statusCode: 0,
                statusMessage: 'Пользователь не зарегистрирован.'
            });
        }
    }
};