const Koa = require('koa');
const bodyParser = require('koa-body');
const app = new Koa();
const router = require('./plugins/router.js');
const errors = require('./plugins/errors.js');
require('./plugins/database.js');
app
    .use(errors)
    .use(bodyParser())
    .use(router.middleware());
app.listen(3000, (err) => {
    err
        ? console.log('Во время запуска произошла ошибка', err)
        : console.log('HTTP сервер запущен на 3000 порту');
});