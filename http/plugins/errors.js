module.exports = async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        // will only respond with JSON
        console.log('IN ОБРАБОТЧИК');
        ctx.status = err.statusCode || err.status || 500;
        ctx.body = {
            message: err.message
        };
    }
};
