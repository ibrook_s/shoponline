const Router = require('koa-trie-router');
const router = new Router();

const User = require('../controllers/User.js');
router
    .get('/api/', (ctx, next) => {
        ctx.body = 'auth';
    })
    .post('/api/register', User.add)
    .post('/api/login', User.login);
module.exports = router;